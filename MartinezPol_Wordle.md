#Projecte Wordle
===
[enllaç al repositori de GitLab](https://gitlab.com/pol.martinez.7e6/m3)

##Com funciona?
---
-Aquest joc consisteix en intoduir una paraula de 5 lletres fins a 6 cops. Si les lletres de la paraula de l'usuari i la paraula secreta coincideixen en la mateixa posicio, el fons de la lletra es posarà de color verd. Si la lletra de la paraula que ha intoduit l'usuari esta en la paraula secreta però no en aquella posició es posarà el fons en color groc. Si la lletra no esta el fons seguira de color gris.

##Objectius del treball
---
-Els objectius d'aquest treball, consisteixen en crear el joc del Wordle, però amb unes funcionalitats menys que les que té el joc real, ja que en el nostre joc podem intoduir la mateixa lletra 5 cops i el programa no ens dirà res. I les lletres repetides fan que tinguem problemes a l'hora d'adivinar la paraula.

##Que he après?
---
Durant l'elaboració d'aquest treball he apres a imprimir les lletres de diferent color i de diferent color de fons, he pogut consolidar l'aprenentatge dels bucles i els condicionals en Kotlin i també l'us de variables booleanes.

##Coses a millorar
---
Principalment hi han dues coses que es podrien millorar en aquest programa. La primera seria el contol de les lletres que estan repetides i el color en que s'imprimeixen i la segona seria el contol de les paraules que s'introdueixen, ja que de moment pots introduir paraules que no existeixen.

##Memoria
---
Al principi del desenvolupament d'aquest joc tenia molt clara l'estructura que habia de tenir el meu programa, a mesura que vaig anar avançantamb el programa em vaig donar compte que no ho podria fer d'aquella forma. Un cop tenia l'estructura ben creada el que mes em va costar va ser la impresio dels colors de fons a pantalla, ja que el color gris no sabia molt bé com imprimir-lo per pantalla, ja que totes les lletres que no estaben a la paraula em sortien en groc, però finalment amb l'ajut d'un company vam poder fer que el color gris s'imprimis per pantalla.